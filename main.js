function update(picker) {
    document.getElementById('rgb').innerHTML = Math.round(picker.rgb[0]) + ', ' + Math.round(picker.rgb[1]) + ', ' + Math.round(picker.rgb[2]);
    document.getElementById("change_color").href = "?r=" + Math.round(picker.rgb[0] * 4.0117) + "&g=" + Math.round(picker.rgb[1] * 4.0117) + "&b=" + Math.round(picker.rgb[2] * 4.0117);
}

$(function() {

    $('#slider').change(function() {
        $.get('index.html', {
            'brightness': $(this).val()

        }).done(function(response) {
            console.log(response);
        });
    });

 var $colorWheel = $('#colorwheel');
    for (var i = 0; i < 360; i++) {
        var color = document.createElement("span")
        color.setAttribute("id", "d" + i)
        color.style.backgroundColor = "hsl(" + i + ", 100%, 50%)"
        color.style.msTransform = "rotate(" + i + "deg)"
        color.style.webkitTransform = "rotate(" + i + "deg)"
        color.style.MozTransform = "rotate(" + i + "deg)"
        color.style.OTransform = "rotate(" + i + "deg)"
        color.style.transform = "rotate(" + i + "deg)"
        $colorWheel.append(color)
    };
	$colorWheel.find('span').click(function(){
	 var color = $(this).css('backgroundColor');
	 var rgb = /rgb\((\d+), (\d+), (\d+)\)/.exec(color);
	 	console.log(rgb);
	 	 var r = rgb[1], g = rgb[2],b = rgb[3];
		$.get('index.html', {
            "r" : r,
            "g" : g,
            "b" : b

        }).done(function(response) {
            console.log(response);
        });
	});
});


