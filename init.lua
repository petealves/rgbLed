wifi.setmode(wifi.STATION)

if file.open("password.txt",r) then
    local username=file.readline();
    local password=file.readline();
    file.close();
    username=string.sub(username,1,#username-1);
    password=string.sub(password,1,#password-1);
    wifi.sta.config(username,password);
else
    print("[cannt open password.txt]");
end
-------------------------------------------------
ledR = 1023;
ledG = 1023;
ledB = 1023;
brightness = 1;

function led(r, g, b)
    pwm.setduty(5, r * brightness)
    pwm.setduty(6, g * brightness)
    pwm.setduty(7, b * brightness)
end

pwm.setup(5, 1000, 1023)
pwm.setup(6, 1000, 1023)
pwm.setup(7, 1000, 1023)
pwm.start(5)
pwm.start(6)
pwm.start(7)
------------------------------------------------

local httpRequest={}
httpRequest["/"]="index.html";
httpRequest["/index.html"]="index.html";
httpRequest["/style.css"]="style.css";
httpRequest["/main.js"]="main.js";


local getContentType={};
getContentType["/"]="text/html";
getContentType["/index.html"]="text/html";
getContentType["/style.css"]="text/css";
getContentType["/main.js"]="main.js";

local filePos=0;

tmr.alarm(1, 1000, 1, function() 
    if wifi.sta.getip() == nil then
        print("[IP unavaiable, waiting.]") 
    else 
        tmr.stop(1)
        print("[Connected, IP is "..wifi.sta.getip().."]");
    end 
end)

if srv then srv:close() srv=nil end
srv=net.createServer(net.TCP)
srv:listen(80,function(conn)
    
    conn:on("receive", function(conn,request)
        print("[New Request]");
        local _, _, method, path, vars = string.find(request, "([A-Z]+) (.+)?(.+) HTTP");
        if(method == nil)then
         _, _, method, path = string.find(request, "([A-Z]+) (.+) HTTP");
        end
        local _GET = {}
        if (vars ~= nil)then
            for k, v in string.gmatch(vars, "(%w+)=(%w+)&*") do
                print("["..k.."="..v.."]");
                _GET[k] = v
            end   
        end
        if getContentType[path] then
            requestFile=httpRequest[path];
            print("[Sending file "..requestFile.."]");            
            filePos=0;
            conn:send("HTTP/1.1 200 OK\r\nContent-Type: "..getContentType[path].."\r\n\r\n");            
        else
            print("[File "..path.." not found]");
            conn:send("HTTP/1.1 404 Not Found\r\n\r\n")
            conn:close();
            collectgarbage();
        end  
    
        if(_GET.on) then
          led(ledR, ledG, ledB)
          print(ledR)
        end
        

        if(_GET.r or _GET.g or _GET.b) then
            led(_GET.r, _GET.g,_GET.b);
            ledR = _GET.r;
            ledG = _GET.g;
            ledB = _GET.b;
        end

        if(_GET.off) then
            led(0,0,0)
        end

        if(_GET.medium) then
            led(ledR*0.4, ledG*0.4, ledB*0.4)
        end

        if(_GET.brightness) then 
            brightness = _GET.brightness * 0.01;
           led(ledR, ledG, ledB);
        end
            


    --close receive
    end)


    conn:on("sent",function(conn)
        if requestFile then
            if file.open(requestFile,r) then
                file.seek("set",filePos);
                local partial_data=file.read(512);
                file.close();
                if partial_data then
                    filePos=filePos+#partial_data;
                    print("["..filePos.." bytes sent]");
                    conn:send(partial_data);
                    if (string.len(partial_data)==512) then
                        return;
                    end
                   
                end
            else
                print("[Error opening file"..requestFile.."]");
            end
        end
        print("[Connection closed]");
        conn:close();
        collectgarbage();
    end)
end)
